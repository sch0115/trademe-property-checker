const axios = require('axios');
const HtmlParser = require('node-html-parser');

const notificationService = require('../notifying.service');
const House = require('../models/house');


module.exports = class RealEstateWeb {
  constructor(name, baseUrl, filterUrl) {
    this.name = name;
    this.baseUrl = baseUrl;
    this.filterUrl = filterUrl;
    this.houses = null;
  }

  async getNewHouses() {
    let newHouses = [];
    try {
      const response = await axios.get(this.filterUrl);
      const htmlWithListings = response.data;
      const houses = this._parseHouses(htmlWithListings, this.name);
      newHouses = this.houses ? houses.filter(house => !this.houses.filter(x => x.id === house.id).length) : [];
      // newHouses = this.houses ? houses.filter(house => this.houses.filter(x => x.id === house.id).length) : [];

      this.houses = houses;
    } catch (error) {
      console.error('Error has happen when checking for houses at: ', Date());
      console.error(error);
      console.error('Checker Will retry soon');
    }
    return newHouses;
  }

  _saveHouses(houses) {
    for (const house of houses) {
      house.save();
    }
  }

  _parseHouses(html, realEstateWebName) {
    switch (realEstateWebName) {
      case 'Trademe':
        return this._parseHousesFromTrademe(html);
      case 'Hardcoats':
        return this._parseHousesFromHardcoats(html);
      case 'Bayleys':
        return this._parseHousesFromBayleys(html);
      default:
        throw 'Parser not implemented for this web';
    }
  }

  _parseHousesFromTrademe(html) {
    const trademeLogo = 'https://trademe.tmcdn.co.nz/tm/kevin/Kevin_20-years-young_FA.png?customkev';
    const houses = [];
    const htmlDoc = HtmlParser.parse(html);
    const propertiesListed = htmlDoc.querySelectorAll('.tmp-search-card-list-view__link');
    for (const property of propertiesListed) {
      const houseName = property.querySelectorAll('.tmp-search-card-list-view__title')[0].innerHTML;
      const url = this.baseUrl + property.rawAttributes.href;
      const address = property.querySelectorAll('.tmp-search-card-list-view__subtitle')[0].innerHTML;
      const coverPicture = property.querySelectorAll('.tmp-search-card-list-view__listing-photo')[0].attributes.src;
      const price = property.querySelectorAll('.tmp-search-card-list-view__price')[0].innerHTML;
      const bedrooms = property.querySelectorAll('.tmp-search-card-list-view__attribute-value')[0].innerHTML;
      const bathrooms = property.querySelectorAll('.tmp-search-card-list-view__attribute-value')[1].innerHTML;
      const garage = '?';
      const house = new House(property.id, url, this.name, trademeLogo, houseName, address, coverPicture, price, bedrooms, bathrooms, garage);

      houses.push(house);
    }
    return houses;
  }
  _parseHousesFromHardcoats(html) {
    const logo = 'https://harcourts.co.nz/Images/Brands/hc_f.png';
    const houses = [];
    const htmlDoc = HtmlParser.parse(html);
    const propertiesListed = htmlDoc.querySelectorAll('#galleryView li');
    for (const property of propertiesListed) {
      const _content = property.querySelector('.listingContent');
      const _spec = property.querySelector('.propFeatures');
      const houseName = _content.querySelectorAll('h2 a')[0].innerHTML;
      const id = property.attributes.listingID;
      const url = _content.querySelectorAll('h2 a')[0].attributes.href;
      const address = _content.querySelectorAll('h3')[0].innerHTML;
      const coverPicture = property.querySelectorAll('img')[0].attributes.src;
      const price = _spec.querySelectorAll('h3')[0].innerHTML;
      const bedrooms = _spec.querySelectorAll('li')[0] ? _spec.querySelectorAll('li')[0].attributes.title : 0;
      const bathrooms = _spec.querySelectorAll('li')[1] ? _spec.querySelectorAll('li')[1].attributes.title : 0;
      const garage = _spec.querySelectorAll('li')[2] ? _spec.querySelectorAll('li')[2].attributes.title : 0;
      const house = new House(id, this.baseUrl + url, this.name, logo, houseName, address, coverPicture, price, bedrooms, bathrooms, garage);
      houses.push(house);
    }
    return houses;
  }
  _parseHousesFromBayleys(html) {
    const bayleysLogo = 'realEstateWebLogo';
    const houses = [];
    const htmlDoc = HtmlParser.parse(html);
    const propertiesListed = htmlDoc.querySelectorAll('.sin-properties');
    for (const property of propertiesListed) {
      const _content = property.querySelector('.content');
      const _spec = property.querySelector('.pro-spec');
      const houseName = property.querySelectorAll('img')[0].attributes.alt;
      const id = houseName;
      const url = property.querySelectorAll('a')[0].attributes.href;
      const address = _content.querySelectorAll('span')[0].innerHTML;
      const coverPicture = property.querySelectorAll('img')[0].attributes.src;
      const price = property.querySelectorAll('.pro-price')[0].innerHTML;
      const bedrooms = _spec.querySelectorAll('span')[0].innerHTML;
      const bathrooms = _spec.querySelectorAll('span')[1].innerHTML;
      const garage = _spec.querySelectorAll('span')[2].innerHTML;
      const house = new House(id, url, this.name, bayleysLogo, houseName, address, coverPicture, price, bedrooms, bathrooms, garage);
      houses.push(house);
    }
    return houses;
  }
};