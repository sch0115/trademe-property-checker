const mongo = require('../util/database');

const collection = "propertyChecker";
const entityType = "house";


module.exports = class House {
  constructor(id, url, realEstateWebName, realEstateWebLogo, name, address, coverPicture, price, bedroom, bathroom, garage) {
    this.id = id;
    this.entityType = entityType;
    this.url = url;
    this.realEstateWebName = realEstateWebName;
    this.realEstateWebLogo = realEstateWebLogo;
    this.name = name;
    this.address = address;
    this.coverPicture = coverPicture;
    this.price = price;
    this.bedroom = bedroom;
    this.bathroom = bathroom;
    this.garage = garage;
    this.added = Date();
    this.checked = false;
  }

  save() {
    const db = mongo.getDb();
    if (db) {
      db.collection(collection)
        .insertOne(this)
        .then(result => {
          console.log('inserted ', this, ' into DB');
        })
        .catch(err => console.log(err));
    } else {
      console.error('Database not connected! Environment data were not saved!')
    }
  }

  static async fetchAll() {
    console.log('Fetching all houses');
    const db = mongo.getDb();
    let allHouses = [];
    if (db) {
      allHouses = await db.collection(collection).find({entityType}).toArray();
    } else {
      console.error('Database not connected! Houses were not fetched!');
    }
    return allHouses;
  }

  static async fetchAllByRealEstateWebName(name) {
    const db = mongo.getDb();
    let selectedHouses = [];
    if (db) {
      selectedHouses = await db.collection(collection).find(
        {
          entityType,
          realEstateWebName: name
        }
      );
      // selectedHouses = allHouses.filter(house => house.name === name);
    } else {
      console.error('Database not connected! Houses by real estate web name were not fetched!');
    }
    return selectedHouses;
  }
}