const MODULE_NAME = 'BookingService';

const SendgridService = require('./sendgrid.service');

const EMAIL_SUBJECT = 'NEW HOUSE detected!';


// const EMAIL_RECIEVER =  'adam.sch0115@sharklasers.com';
const EMAIL_RECIEVER =  'adam.sch0115@gmail.com';

const EMAIL_RECIPIENTS = [
  {
    "email": 'adam.sch0115@gmail.com'
  },
  {
    "email": 'radka.kubonova@gmail.com'
  },
  {
    "email": 'marek.mitana@gmail.com'
  }];






module.exports =  {
  name: MODULE_NAME,

  notify(newHouses) {
    sendNotificationEmail(newHouses);
    console.log('Sending the email about new houses');
  }
};

async function sendNotificationEmail(newHouses) {
  await SendgridService.sendEmail(EMAIL_RECIPIENTS, 'newHouses@christchurch.nz', EMAIL_SUBJECT, formatHouses(newHouses));
  console.log('Email sent');
}

function formatHouses(houses) {
  let housesString = '';
  for (const house of houses) {
    housesString += '===================\n';
    housesString += JSON.stringify(house);
    housesString += '\nUrl: ' + house.url;
    housesString += '\n===================';
  }
  return housesString;
}

