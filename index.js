var CronJob = require('cron').CronJob;
const axios = require('axios');
const mongo = require('./src/util/database');

const RealEstateWeb = require('./src/models/realEstateWeb');
const House = require('./src/models/house');
const trademeURL = 'https://www.trademe.co.nz/Browse/CategoryAttributeSearchResults.aspx?sort_order=expiry_desc&search=1&cid=5748&sidebar=1&132=FLAT&selected135=60&134=3&135=60&216=0%2C0&217=0%2C0&153=&122=2%2C6&123=1%2C6&59=30000%2C0&178=0%2C0&sidebarSearch_keypresses=0&sidebarSearch_suggested=0&rsqid=81420fc8d6964c89b63c77ca83268414';
const hardcoatsURL = 'https://harcourts.co.nz/Property/Rentals?pageid=-2&search=&formsearch=true&OriginalTermText=&OriginalLocation=&location=25007&proptype=&min=300&max=&minbed=2&maxbed=&view=gallery&results=30&page=1';
const bayleysURL = 'https://bayleyspm.co.nz/christchurch-rentals/?streetsuburbRentals=&bedrooms[]=2&bedrooms[]=3&bedrooms[]=4&bedrooms[]=5&bedrooms[]=6&bedrooms[]=7&minpricer=300&maxpricer=99999&qt=search&useSearchType=search&orderBy=sortRecent';
const trademe = new RealEstateWeb('Trademe', 'https://www.trademe.co.nz', trademeURL);
const hardcoats = new RealEstateWeb('Hardcoats', 'https://www.harcourts.co.nz', hardcoatsURL);
const bayleys = new RealEstateWeb('Bayleys', 'https://www.bayleyspm.co.nz', bayleysURL);


const houseCheck = new CronJob('*/59 * * * * *', function() {
  let newHouses = [];
  try {
    newHouses.concat(bayleys.getNewHouses());
  } catch (e) {
    console.log(e);
  }
  try {
    newHouses.concat(trademe.getNewHouses());
  } catch (e) {
    console.log(e);
  }
  try {
    newHouses.concat(hardcoats.getNewHouses());
  } catch (e) {
    console.log(e);
  }
  for (const house in newHouses) {
    house.save();
  }
}, null, true);


mongo.mongoConnect(async () => {
  console.log('Database connected')
  houseCheck.start();
  console.log('Starting regular 1 minute check...');
  console.log('Checking for new houses...');
});
